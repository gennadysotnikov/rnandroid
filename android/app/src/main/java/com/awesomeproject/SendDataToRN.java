package com.awesomeproject;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;


import androidx.annotation.NonNull;


public class SendDataToRN extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    SendDataToRN(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @ReactMethod
    public void sum(int a, int b, Callback errorCallback, Callback successCallback) {
        try {
            successCallback.invoke((a+b));
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @NonNull
    @Override
    public String getName() {
        return "SendDataToRN";
    }

}

